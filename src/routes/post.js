const router = require('express').Router();
const postController = require('../controllers/postController');
const authMiddleware = require('../middlewares/auth');
const adminMiddleware = require('../middlewares/admin');

// console.log('2')
router.get('/posts', postController.index);
router.get('/posts/search/:regex', postController.search);
router.post('/posts', authMiddleware, postController.create);
router.put('/posts/:id', authMiddleware, postController.update);
router.get('/posts/:id', postController.findPostById);
router.delete('/posts/:id', [authMiddleware, adminMiddleware], postController.delete);

module.exports = router;
