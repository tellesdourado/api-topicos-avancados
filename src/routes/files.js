const router = require("express").Router();
const authMiddleware = require("../middlewares/auth");
const uploadMiddleware = require('../middlewares/file')
const filesController = require('../controllers/filesController');


router.get("/files/:id", filesController.download);



router.post("/files", [authMiddleware, uploadMiddleware.single("file")], filesController.create);
router.get("/files", authMiddleware, filesController.index)
// router.get("/files/:id", authMiddleware, filesController.findUserById);

// router.get("/files/download/:id", authMiddleware, filesController.findUserById);

router.delete("/files/:id", authMiddleware, filesController.delete);

module.exports = router;
