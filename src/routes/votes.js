const router = require('express').Router();
const votesController = require('../controllers/votesController');
const authMiddleware = require('../middlewares/auth');
const adminMiddleware = require('../middlewares/admin');

router.post('/votes', [authMiddleware], votesController.create);
router.get('/vote/:post_id', [authMiddleware], votesController.index);
router.get('/votes/:post_id', votesController.findAll);
router.delete('/votes/:id', [authMiddleware], votesController.delete);

module.exports = router;
