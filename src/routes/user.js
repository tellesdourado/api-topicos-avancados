const router = require('express').Router();
const authMiddleware = require('../middlewares/auth');
const userController = require('../controllers/usersController');

router.get('/current', authMiddleware, userController.current);
router.put('/user', authMiddleware, userController.update);

module.exports = router;
