const router = require('express').Router();
const comentsController = require('../controllers/comentsController');
const authMiddleware = require('../middlewares/auth');
const adminMiddleware = require('../middlewares/admin');

router.post('/coments', authMiddleware, comentsController.create);

router.get('/coments/:post_id', comentsController.findComentsByPost);

router.delete('/coments/:id', [authMiddleware], comentsController.delete);

module.exports = router;
