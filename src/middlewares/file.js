const multer = require('multer');
const fs = require('fs');
const path = require('path');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    // console.log();
    const dir = `${path.join(__dirname, '../')}uploads/`;
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
      cb(null, dir);
    } else {
      cb(null, dir);
    }
  },
  filename: (req, file, cb) => {
    cb(null, `${Date.now()}-${file.originalname}`);
  }
});

module.exports = multer({ storage });
