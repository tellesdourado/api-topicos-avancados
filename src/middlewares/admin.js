

module.exports = (req, res, next) => {
    const { role } = req;
    console.log(role)
    if (role == 'admin') return next();

    res.status(403).send({message:"not authorized"})
}