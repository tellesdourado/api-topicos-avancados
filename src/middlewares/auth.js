const jwt = require("jsonwebtoken");


module.exports = (req, res, next ) => {
    const authHeader = req.headers.authorization;
        // console.log(authHeader)
        // console.log('1')
    if(!authHeader)
        return res.status(401).send({ error: "No token provided" })
    
    const parts = authHeader.split(' ');

    if(!parts.length === 2)
        return res.status(401).send({ error: "Token error" })
    
    const [ scheme, token ] = parts;
    // console.log(parts)
    if(scheme !== "Bearer")
        return res.status(401).send({ error: "Token malformated" })
    
    jwt.verify(token,"1d11a4e6e77ef385c5fbb881e40ca2c4",(err, decoded)=>{
        if(err)
            return res.status(401).send({ error: "Token Invalid" })
        
        req.userId = decoded.id;
        req.role = decoded.role;
    
        
        return next();
    })
    
}