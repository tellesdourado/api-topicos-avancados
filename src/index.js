const express = require('express');
require('dotenv').config();
const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors = require('cors');
const app = express();
const path = require('path');

const requireDirectory = require('require-directory');
const { PORT, MORGAN } = process.env;

app.use(express.json());
app.use(cors());

if (MORGAN) app.use(morgan('dev'));

app.use('/images', express.static(path.join(__dirname, 'uploads')));

let routes = requireDirectory(module, './routes/');

Object.values(routes).map(route => app.use(route));

app.listen(PORT || 3000);

app.use((req, res, next) => {
  res.status(404).send({ message: 'page note found' });
  next();
});

module.exports = app;
