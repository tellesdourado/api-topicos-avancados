const mongoose = require("../database");

const VotesSchema = mongoose.Schema({
    vote: {
        type: Number,
        required: true,
    },
    post_id:{
        type: String,
        required: true,
    },
    user_id: {
        type: String,
        required: true,
    },
    timestamp: {
        type: String,
        default: Date.now
    }
});


module.exports = mongoose.model("votes", VotesSchema);
