const mongoose = require("../database");
const bcrypt = require("bcryptjs");
const UserSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true,
        lowercase: true
    },
    password: {
        type: String,
        required: true,
        select: false
    },
    role: {
        type: String,
        default: "user"
    },
    disabled: {
        type: Boolean,
        default: false
    },
    createdAt: {
        type: String,
        default: Date.now(),
    }
})



UserSchema.pre("save", async function (next) {
    const hash = await bcrypt.hash(this.password, 10)
    this.password = hash;
    if (await mongoose.model("User", UserSchema).findOne({ role: this.role }))
        this.role = "user";
    next();
})


module.exports = mongoose.model("User", UserSchema);