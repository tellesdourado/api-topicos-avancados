const mongoose = require('../database');

const PostSchema = mongoose.Schema({
  sinopse: {
    type: String,
    required: true
  },
  link: {
    type: String,
    required: true
  },
  title: {
    type: String,
    required: true
  },
  user_id: {
    type: String,
    required: true
  },
  banner_id: {
    type: String,
    required: true
  },
  timestamp: {
    type: String,
    default: Date.now
  }
});

module.exports = mongoose.model('posts', PostSchema);
