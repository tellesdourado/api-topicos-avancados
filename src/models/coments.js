const mongoose = require("../database");

const MessageSchema = mongoose.Schema({
    message: {
        type: String,
        required: true,
    },
    user_id: {
        type: String,
        required: true,
    },
    post_id:{
        type: String,
        required: true,
    },
    timestamp: {
        type: String,
        default: Date.now
    }
});


module.exports = mongoose.model("messages", MessageSchema);



