const mongoose = require('../database');

const FileSchema = mongoose.Schema({
  filename: {
    type: String
    // required: true,
  },
  type: {
    type: String,
    required: true
  },
  size: {
    type: String,
    required: true
  },
  savedname: {
    type: String,
    required: true
  },
  id_user: {
    type: String,
    required: true
  },
  timestamp: {
    type: String,
    default: Date.now
  }
});

module.exports = mongoose.model('file', FileSchema);
