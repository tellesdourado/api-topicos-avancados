const postModel = require('../models/posts');

module.exports = {
  async index(req, res) {
    const rows = await postModel.find({});
    return res.send(rows);
  },
  async create(req, res) {
    const { sinopse, banner_id, link, title } = req.body;
    const { userId: user_id } = req;
    if (!sinopse || !banner_id || !link || !title)
      return res.send({ message: 'missing parameters' });

    const field = await postModel.create({
      sinopse,
      user_id,
      banner_id,
      title,
      link
    });
    return res.send(field);
  },
  async findPostById(req, res) {
    const { id } = req.params;

    const row = await postModel.findById(id);

    return res.send(row);
  },
  async update(req, res) {
    const { id } = req.params;
    const { userId: user_id } = req;
    const { sinopse, banner_id, link, title } = req.body;

    // if (!sinopse || !banner_id || !link || !title)
    //   return res.send({ message: 'missing parameters' });

    const field = await postModel.findByIdAndUpdate(id, req.body);
    return res.status(200).send(field);
  },
  async delete(req, res) {
    const { id } = req.params;
    try {
      if (await postModel.findByIdAndDelete(id))
        return res.status(200).send({
          message: 'post deleted'
        });

      return res.status(401).send({
        message: 'post is not deleted'
      });
    } catch (e) {
      return res.status(401).send({
        message: 'post is not deleted'
      });
    }
  },
  async search(req, res) {
    const { regex } = req.params;

    try {
      const title = new RegExp(regex, 'i');
      return res.status(200).send(await postModel.find({ title }));
    } catch (e) {
      return res.status(400).send({ error: 'unable to search', error: e });
    }
  }
};
