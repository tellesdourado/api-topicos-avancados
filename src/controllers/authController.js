const User = require('../models/user');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

module.exports = {
  async register(req, res, next) {
    const { email } = req.body;

    try {
      if (await User.findOne({ email }))
        return res.status(400).send({ error: 'User already existes' });

      const user = await User.create(req.body);
      user.password = undefined;
      return res.send(user);
    } catch (err) {
      return res.status(400).send({ error: 'Registration failed' });
    }
  },
  async autenticate(req, res, next) {
    const { email, password } = req.body;

    let user = await User.findOne({ email }).select('+password');
    if (!user) return res.status(401).send({ error: 'User not Found.' });

    if (!(await bcrypt.compare(password, user.password)))
      return res.status(400).send({ error: 'Password Invalid' });

    user.password = undefined;

    console.log(user);

    const token = jwt.sign({ id: user.id, role: user.role }, '1d11a4e6e77ef385c5fbb881e40ca2c4', {
      expiresIn: 86400
    });

    return res.send({ user, token });
  }
};
