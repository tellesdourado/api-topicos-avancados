const comentsModel = require('../models/coments');
const userModel = require('../models/user');

module.exports = {
  async create(req, res) {
    const { message, post_id } = req.body;
    const { userId } = req;
    if (!message || !post_id) return res.send({ message: 'missing params' });

    const field = await comentsModel.create({ message, user_id: userId, post_id });
    return res.send(field);
  },
  async findComentsByPost(req, res) {
    const { post_id } = req.params;

    const row = await comentsModel.find({ post_id });

    const promisse = row.map(async doc => {
      let user = await userModel.findById(doc.user_id);
      return { ...doc._doc, name: user._doc.name };
    });

    const posts = await Promise.all(promisse);

    res.status(200).send(posts);
  },

  async delete(req, res) {
    const { userId: user_id } = req;
    try {
      const { id } = req.params;
      // console.log(user_id);
      if (await comentsModel.findOneAndRemove({ _id: id, user_id }))
        return res.status(200).send({ message: 'coment deleted' });

      return res.status(400).send({ message: 'you are unable to delete this coment ' });
    } catch (e) {
      return res.status(400).send({ message: 'no coment deleted' });
    }
  }
};
