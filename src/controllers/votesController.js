const votesModel = require('../models/votes');

module.exports = {
  async create(req, res) {
    const { vote, post_id } = req.body;
    const { userId: user_id } = req;

    if (!vote || !post_id) return res.send({ message: 'missing params' });

    if (!Number.isInteger(vote))
      return res.status(401).send({ message: 'vote need to be a number' });

    if (vote > 5 || vote < 0)
      return res.status(401).send({ message: 'votes need to be between 0 and 5' });

    const voted = await votesModel.findOne({ user_id, post_id });

    if (!voted) {
      let field = await votesModel.create({ vote, user_id, post_id });
      return res.send(field);
    } else {
      await votesModel.findByIdAndUpdate(voted._id, { vote });
      return res.status(200).send(await votesModel.findById(voted._id));
    }
  },
  async index(req, res) {
    const { userId: user_id } = req;
    const { post_id } = req.params;

    const row = await votesModel.findOne({ post_id, user_id });
    if (row) return res.status(200).send(row);

    return res.status(200).send({ message: 'waiting for vote' });
  },

  async findAll(req, res) {
    const { post_id } = req.params;

    const row = await votesModel.find({ post_id });

    let total = 0;
    row.map(item => (total += item.vote));
    return res.status(200).send({ media: total / row.length, quantidade: row.length });
  },

  async delete(req, res) {
    const { userId: user_id } = req;
    try {
      const { id } = req.params;
      // console.log(user_id);
      if (await comentsModel.findOneAndRemove({ _id: id, user_id }))
        return res.status(200).send({ message: 'coment deleted' });

      return res.status(400).send({ message: 'you are unable to delete this coment ' });
    } catch (e) {
      return res.status(400).send({ message: 'no coment deleted' });
    }
  }
};
