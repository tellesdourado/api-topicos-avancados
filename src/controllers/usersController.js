const userModel = require('../models/user');
const bcrypt = require('bcryptjs');

module.exports = {
  async current(req, res) {
    const { userId } = req;
    return res.status(200).send(await userModel.findById(userId));
  },
  async update(req, res) {
    const { password, name } = req.body;
    const { userId } = req;
    const body = {
      password,
      name
    };

    if (!password && !name) return res.status(400).send({ message: 'missing parameters' });
    try {
      const user = await userModel.findById(userId).select('+password');
      if (!password) {
        body.password = user.password;
      } else {
        body.password = await bcrypt.hash(password.toString(), 10);
      }

      if (!name) {
        body.name = user.name;
      }
      await userModel.findByIdAndUpdate(userId, body);
      return res
        .status(200)
        .send({ message: 'user updated', ...(await userModel.findById(userId))._doc });
    } catch (err) {
      return res.status(400).send({ error: 'Registration failed' });
    }
  }
};
