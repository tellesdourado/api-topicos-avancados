const fileModel = require('../models/file');
const path = require('path');
const fs = require('fs');

module.exports = {
  async create(req, res) {
    try {
      let { originalname, mimetype, size, filename } = req.file;

      if (!originalname) originalname = `file`;
      let create = await fileModel.create({
        filename: originalname,
        type: mimetype,
        size: size,
        id_user: req.userId,
        savedname: filename
      });
      if (create) {
        res.send(create);
      } else {
        res.status(401).send({ error: 'file not saved' });
      }
    } catch (e) {
      res.status(403).send({ e });
    }
  },
  async index(req, res) {
    let files = await fileModel.find({ id_user: req.userId });
    if (files) {
      return res.status(200).send(files);
    } else {
      return res.status(200).send({ status: 'user not have any file' });
    }
  },
  async findUserById(req, res) {
    let files = await fileModel.find({ id_user: req.params.id });
    if (files) {
      return res.status(200).send(files);
    } else {
      return res.status(200).send({ status: 'user not have any file' });
    }
  },
  async download(req, res) {
    const { id } = req.params;
    const row = await fileModel.findById(id);
    res.status(200).send({ path: `/images/${row.savedname}`, ...row._doc });
  },

  async delete(req, res) {
    const { id } = req.params;
    try {
      const file = await fileModel.findById(id);

      if (file) {
        fs.unlink(`${path.join(__dirname, '../')}uploads/${file.savedname}`, async err => {
          if (err) {
            return res.status(401).send({ error: 'no file deleted' });
          }
          await fileModel.deleteOne({ _id: id });
          return res.status(200).send({ message: 'file deleted' });
        });
      } else {
        return res.status(404).send({ message: 'file not found' });
      }
    } catch (e) {
      console.log(e);
    }
  }
};
