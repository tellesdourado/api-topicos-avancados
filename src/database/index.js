const mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
mongoose.connect("mongodb://ec2-3-19-75-83.us-east-2.compute.amazonaws.com/api-vanessa", { useNewUrlParser: true, useFindAndModify: true, useUnifiedTopology: true })

mongoose.Promise = global.Promise;

module.exports = mongoose;