module.exports = {
    apps: [
        {
            name: "api.nuvi.center",
            script: "./src/index.js",
            watch: true,
            env: {
                "PORT": 9000,
                "NODE_ENV": "development"
            },
            env_production: {
                "PORT": 9000,
                "NODE_ENV": "production",
            }
        }
    ]
}